#!/bin/bash

### PROPIEDADES DEPENDIENTES DEL ENTORNO ###

CATALOGUE_URL="https://registry.erasmuswithoutpaper.eu/catalogue-v1.xml"
KEYSTORE="/etc/apps/ewpcv/ewp_trusted.jks"
KEYSTORE_PASSWORD="xxxxxxx"

# Ficheros/directorios temporales
HOST_LIST="/var/tmp/hosts.lst.$$"
CERTIFICATES_FOLDER="/var/tmp/certs.$$"
CATALOGUE_FILE="/var/tmp/catalogue.xml.$$"

echo "***************************************************"
echo "* Start: $(date)"
echo "***************************************************"

# Descarga el catálogo
echo "*** Descargando catálogo desde" $CATALOGUE_URL
curl --silent -k $CATALOGUE_URL > $CATALOGUE_FILE
echo "*** Catálogo descargado en" $CATALOGUE_FILE

# Busca el namespace en el que están las URLs (ahora mismo conviven la v5 y la v6)
URL_NAMESPACE_XSD="https://github.com/erasmus-without-paper/ewp-specs-api-discovery/blob/stable-v[56]/manifest-entry.xsd"
URL_NAMESPACE_REGEX="xmlns:.+=\"${URL_NAMESPACE_XSD}\""
URL_NAMESPACE_ALT=`grep -o -E $URL_NAMESPACE_REGEX $CATALOGUE_FILE | cut -d "=" -f 1 | cut -d ":" -f 2 | paste -sd '|'`
URL_NAMESPACE="(${URL_NAMESPACE_ALT})"

# Busca las URLs de los manifests. Algunas pueden aparecer repetidas.
echo "*** Buscando URLs en el catálogo..."
URL_REGEX="<${URL_NAMESPACE}:url>.+</${URL_NAMESPACE}:url>"
MANIFEST_LIST=`grep -o -E $URL_REGEX $CATALOGUE_FILE | sort | uniq | cut -d ">" -f 2 | cut -d "<" -f 1`

# Añadimos la URL del propio catálogo
MANIFEST_LIST+=$'\n'${CATALOGUE_URL}

echo "***" $(echo "$MANIFEST_LIST" | wc -l) "URLs distintas encontradas"

# Crea (o vacía) la lista de hosts
echo "*** Creando lista de hosts en" $HOST_LIST
>$HOST_LIST

# Extrae el host y el puerto de cada URL de manifest
for line in $MANIFEST_LIST
do
    # Asumimos que todas las URL están bien formadas
    prefix=${line%://*}
    line_no_prefix=${line#*://}
    host_with_port=${line_no_prefix%%/*}

    # El puerto puede (o no) aparecer en el host
    port=${host_with_port#*:}
    # Si el puerto NO aparece, esta variable es igual al host, y calculamos el puerto a partir del prefijo.
    if [[ "$port" == "$host_with_port" ]];
    then
        if [[ "$prefix" == "https" ]];
        then
            port=443
        else
            port=80
        fi
        host=${host_with_port}
    else
        host=${host_with_port%:*}
    fi

    echo $host,$port >> $HOST_LIST
done

# Quita duplicados innecesarios
sort $HOST_LIST | uniq > tmp && mv tmp $HOST_LIST
echo "*** $(wc -l $HOST_LIST | cut -d ' ' -f1) hosts distintos encontrados" 

# Crea el directorio para los certificados en caso de que no exista
echo "*** Creando carpeta de certificados en:" $CERTIFICATES_FOLDER
mkdir -p $CERTIFICATES_FOLDER

# Descarga el certificado de cada uno de los hosts y lo importa al JKS
KEYSTORE_CHANGED=0
echo "*** Descargando el certificado de cada host e importando al JKS" $KEYSTORE
for value in $(cat $HOST_LIST); do
    HOST=${value%,*}
    PORT=${value##*,}
    echo -n "*** $HOST $PORT ... "

    CERTIFICATE_FILE=$CERTIFICATES_FOLDER/$HOST.der
    timeout 10 openssl s_client -showcerts -servername $HOST -connect $HOST:$PORT 2>/dev/null </dev/null | openssl x509 -outform DER > $CERTIFICATE_FILE
    if [[ -s ${CERTIFICATE_FILE} ]]; then
        CERTIFICATE_FINGERPRINT=$(openssl x509 -inform DER -noout -in $CERTIFICATE_FILE -fingerprint -sha256 | awk -F= '$0 ~ /^(sha|SHA)256 Fingerprint/ { print $NF }')
        CERTIFICATE_FINGERPRINT_IN_JKS=$(keytool -list -alias $HOST -keystore $KEYSTORE -storetype JKS -storepass $KEYSTORE_PASSWORD -trustcacerts -noprompt | awk '$0 ~ /^Certificate fingerprint/ { print $NF }')

        if [[ "$CERTIFICATE_FINGERPRINT" == "$CERTIFICATE_FINGERPRINT_IN_JKS" ]]; then
            echo "EXISTS IN JKS"
        else
            if [[ -n $CERTIFICATE_FINGERPRINT_IN_JKS ]]; then
	        echo -n "DELETED, "
                keytool -delete -alias $HOST -keystore $KEYSTORE -storetype JKS -storepass $KEYSTORE_PASSWORD
            fi
	    echo "IMPORTED"
	    keytool -import -file $CERTIFICATE_FILE -alias $HOST -keystore $KEYSTORE -storetype JKS -storepass $KEYSTORE_PASSWORD -trustcacerts -noprompt
            if [[ $? -eq 0 ]]; then
                KEYSTORE_CHANGED=1
	    fi
        fi
    else
        echo "SKIPPED (cannot be downloaded)"
    fi
done

# Reinicio de las instancias en caso necesario
if [[ $KEYSTORE_CHANGED -ne 0 ]]; then
    echo "Aquí puede ir algún script para reiniciar los nodos para que el conector pille los cambios en el JKS"
else
	echo "Keystore not changed, skipping restart"
fi

# Limpieza de temporales
rm -rf $CATALOGUE_FILE $HOST_LIST $CERTIFICATES_FOLDER 

echo "***************************************************"
echo "* End: $(date)"
echo "***************************************************"

exit 0