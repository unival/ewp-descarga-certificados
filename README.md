# Descarga de certificados EWP

Permite la generación de un keystore con todos los certificados de host SSL utilizados por las universidades dadas de alta en el registry del EWP.

El script Bash se encarga de realizar la siguiente secuencia de acciones: 

1. Descarga el catálogo del registro
2. Busca las urls del discovery de todos los nodos ya que es obligatorio implementar esta API.
3. Crea un fichero donde volcar los hosts y los puertos de cada una de las urls encontradas. Incluye además la del propio registro a consultar.
4. Quita los duplicados del fichero generado.
5. Para cada Host descarga la clave pública del certificado y extrae su fingerprint
6. Busca en el almacen de claves JKS* un certificado con alias asociado al nombre del host y extrae su fingerprint.**
    > '*' El script esta preparado para trabajar con ficheros JKS si deseamos trabajar con ficheros P12 deberemos adecuar los comandos a este formato.

    > '**' Este proceso se realiza con un pipe en base al mensaje arrojado por un comando por lo que es sensible al idioma configurado en la maquina donde se ejecuta el script.
7. Realiza una comparación de fingerprints

    * si son iguales quiere decir que el certificado ya existe en el almacen de claves y no se realiza ninguna acción.
    * si no son iguales pero se ha encontrado un fingerprint para ese alias en el almacen de claves es que lo han actualizado por lo que se elimina el ya existente e inserta el nuevo
    * si no se ha encontrado un fingerprint para ese alias en el almacen de claves simplemente se inserta el nuevo.
8. Reinicia la instancia del conector ewp interoperabilidad.*
    > '*' El script proporcionado no realiza esta acción simplemente habilita un espacio donde incluir los comandos especificos de cada instalación.
9. Limpia los ficheros temporales generados para el procesamiento de los certificados.

  
## Cómo utilizar los scripts desde Linux:

Para poder ejecutar el proceso debemos seguir los siguientes pasos

- Descargar el código desde Git.
- El proceso parte de la existencia previa de un almacen de claves JKS por lo que en caso de no disponer de uno deberemos crearlo.
- Configuraremos las variables al inicio del script indicando:
    - URL del registro a consultar (puede ser la de produccion o la de desarollo)
    - Ruta al keystore que almacena los certificados. 
    - Contraseña del almacen de claves.
    - Rutas a los ficheros temporales (una vez finalizado el proceso serán eliminados).
- Completaremos el bloque asociado al paso número 8 para que nuestra instancia se reinicie tras el cambio del almacen de claves y los cambios se hagan efectivos. En caso de apuntar a un almacen de claves distinto al referenciado por el conector debemos asegurarnos que se copia/mueve al directorio adecuado antes de reiniciar las instancias.
- Ejecutaremos el proceso. Se recomiendo que este paso se realice de forma programtica y periodica empleando algun sistema de automatización.


```bash
./extract-ewp-hosts-from-registry.sh
```

> **NOTA**: No es recomendable importar los certificados directamente al cacerts de Java, ya que ante cualquier actualización del JDK o cambio de versión sería necesario volver a realizar el proceso.